       program outputdata   
       implicit none

       character*20 :: inputFile = "data1.in"
       integer :: status, i, j, matrices, rows, columns
       real :: element, rowSum, overallSum
       real, dimension(:, :), allocatable :: x
       
       write(*, *) 'Enter an input file name.'
       read(*, '(A)') inputFile
       
       open(unit = 1, file =inputFile, status='old', iostat=status)
       open(2, file = 'data1.out', status = 'new')  
       
       read(1, *) matrices
       read(1, *) rows, columns
       
       write(2, *) matrices
       write(2, *) rows, columns
       
       allocate ( x(columns, rows) ) 
       
       read(1, *) x

       do i=1,rows
       rowSum = 0;
         do j=1,columns
           
           rowSum = rowSum + x(j,i)
           
           print *, x(j,i)
         end do
         print *, rowSum
         overallSum = overallSum + rowSum
         
       end do
       
       print *, overallSum
       write(2, *) overallSum

       close(1) 
       close(2)
       deallocate(x)
   
       end program outputdata
