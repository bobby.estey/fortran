       program outputdata   
       implicit none

       real, dimension(100) :: x, y  
       real, dimension(100) :: p, q
       integer :: i  
       integer :: attribute1, attribute2, attribute3
       real :: real1, real2, real3, real4
   
       ! data  
       do i=1,100  
         x(i) = i * 0.1 
         y(i) = sin(x(i)) * (1-cos(x(i)/3.0))  
       end do  
       
       open (unit = 1, file = "data1.in")
       open(2, file = 'data1.out', status = 'new')  
       
       read(1, *) attribute1
       read(1, *) attribute2, attribute3
       read(1, *) real1, real2, real3, real4
       
       write(2, *) attribute1
       write(2, *) attribute2, attribute3
       write(2, *) real1, real2, real3, real4
   
       ! output data into a file 
*      open(2, file = 'data1.out', status = 'new')  
*       do i=1,100  
*         write(2,*) x(i), y(i)   
*       end do  
   
       close(1) 
       close(2)
   
       end program outputdata
