       program inputOutputData   
       implicit none

       integer :: i, int1, int2
       real :: real1, real2, real3, real4
       
       ! open input file
       open (unit = 1, file = "data.in")
       
       ! create output file
       open(2, file = 'data.out', status = 'new')  
       
       read(1,*)   int1
       write(2, *) int1
       
       read(1,*)   int1, int2
       write(2, *) int1, int2
       
       read(1,*)   real1, real2, real3, real4
       write(2, *) real1, real2, real3, real4
   
       close(1) 
       close(2)
   
       end program inputOutputData